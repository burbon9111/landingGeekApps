(function () {
    'use strict';

    angular
        .module('app.terms-and-conditions')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.terms-and-conditions', {
                    url: "/terms-and-conditions",
                    views: {
                        'content@app': {
                            templateUrl: "app/terms-and-conditions/terms-and-conditions.template.html",
                        }
                    }
                });
        })
})();
