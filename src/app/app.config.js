(function () {
    'use strict';

    angular
      .module('app')
      .run(['$rootScope' , 'translation', 'localstorage', 'defaultLang', 'Analytics',
        function ($rootScope, translation, localstorage, defaultLang, Analytics) {
          let defaulLang = defaultLang;

          $rootScope.currentLang = localstorage.get('lang') || defaulLang;
          translation.validateLang($rootScope.currentLang, defaultLang);
          translation.checkIsRTL($rootScope.currentLang);
      }])

      .config(function ($locationProvider, AnalyticsProvider) {
          $locationProvider.hashPrefix('');
          AnalyticsProvider.setAccount('UA-96264765-1');
          AnalyticsProvider.setPageEvent('$stateChangeSuccess');
      })

})();
