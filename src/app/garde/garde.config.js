(function () {
    'use strict';

    angular
        .module('app.garde')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.garde', {
                    url: "/garde",
                    views: {
                        'content@app': {
                            templateUrl: "app/garde/garde.template.html",
                        }
                    }
                });
        })
})();
