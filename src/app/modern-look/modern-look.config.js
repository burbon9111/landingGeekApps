(function () {
    'use strict';

    angular
        .module('app.modern-look')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.modern-look', {
                    url: "/modern-look",
                    views: {
                        'content@app': {
                            templateUrl: "app/modern-look/modern-look.template.html",
                        }
                    }
                });
        })
})();
