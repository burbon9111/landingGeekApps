(function () {
    'use strict';

    angular
        .module('app.layout')
        .config(function ($stateProvider, $urlRouterProvider, $qProvider) {
          $qProvider.errorOnUnhandledRejections(false)
            $urlRouterProvider.otherwise('/landing');

            $stateProvider.state('app', {
                abstract: true,
                views: {
                    '@': {
                        templateUrl: "app/_layout/layout.template.html",
                        controller: "LayoutController"
                    },
                    'header@app': {
                        //templateUrl: 'app/_header/header.template.html',
                        //controller: 'HeaderController',
                        //controllerAs: 'header',
                    },
                    'footer@app': {
                      //  templateUrl: 'app/_footer/footer.template.html'
                    }
                }
            });
        });
})();
