(function(){
    'use strict';

    angular
      .module('app.explore')

      .directive('videoPreview', ['$parse', function ($parse) {
         return {
            restrict: 'A',
            link: function link(scope, elem, attrs, ctrl) {
              elem.bind("click", function(e){
                $(this).addClass('play');
              });
            }
          };
        }])
})();
