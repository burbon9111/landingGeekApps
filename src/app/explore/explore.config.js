(function () {
    'use strict';

    angular
        .module('app.explore')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.explore', {
                    url: "/explore",
                    views: {
                        'content@app': {
                            templateUrl: "app/explore/explore.template.html",
                            controller: "ExploreController",
                            controllerAs: "explore"
                        }
                    }
                });
        })
})();
