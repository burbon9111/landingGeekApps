(function () {
    'use strict';

    angular
        .module('app.web-app-templates-new')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.web-app-templates-new', {
                    url: "/web-app-templates-new",
                    views: {
                        'content@app': {
                            templateUrl: "app/web-app-templates-new/web-app-templates-new.template.html",
                        }
                    }
                });
        })
})();
