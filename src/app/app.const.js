(function () {
    'use strict';

    angular
      .module('app')

      .constant('Theme', {
        Colors: {
          0: '#45b6ed',
          1: '#d4106b',
          2: '#fc701b',
          3: '#f63054',
          4: '#4cba28'
        }
      })

      .constant('Languages', [
        {
          'code': 'en',
          'title': 'English',
          'icon': '../assets/images/languages/gb.png',
          'rtl': false
        },
        {
          'code': 'he',
          'title': ' עברית',
          'icon': '../assets/images/languages/isr.png',
          'rtl': true
        },
        {
          'code': 'ru',
          'title': 'Русский',
          'icon': '../assets/images/languages/rus.png',
          'rtl': false
        },
        {
          'code': 'fr',
          'title': 'Français',
          'icon': '../assets/images/languages/fra.png',
          'rtl': false
        },
        {
          'code': 'ar',
          'title': 'العربية',
          'icon': '../assets/images/languages/oae.png',
          'rtl': true
        },
        {
          'code': 'es',
          'title': 'Español',
          'icon': '../assets/images/languages/spn.png',
          'rtl': false
        },
        {
          'code': 'ch',
          'title': '中文',
          'icon': '../assets/images/languages/chi.png',
          'rtl': false
        },
        {
          'code': 'de',
          'title': 'Deutsch',
          'icon': '../assets/images/languages/ger.png',
          'rtl': false
        },
        {
          'code': 'pl',
          'title': 'Polski',
          'icon': '../assets/images/languages/pl.png',
          'rtl': false
        },
        {
          'code': 'ua',
          'title': 'Українська',
          'icon': '../assets/images/languages/ua.png',
          'rtl': false
        },
        {
          'code': 'pt',
          'title': 'Português',
          'icon': '../assets/images/languages/pot.png',
          'rtl': false
        },
        {
          'code': 'it',
          'title': 'Italiano',
          'icon': '../assets/images/languages/ita.png',
          'rtl': false
        },
        {
          'code': 'ro',
          'title': 'Română',
          'icon': '../assets/images/languages/romana.png',
          'rtl': false
        },
      ])


      .constant('server', {
        //  url: 'http://qa.apps.artygeek.net/api/',
        //  createApp: 'http://qa.apps.artygeek.net/api/application/create',
        //  admin: 'http://qa.apps.artygeek.net/admin/#/loginFromLanding'
         url: 'https://geekapps.com/api/',
         createApp: 'https://geekapps.com/api/application/create',
         admin: 'https://geekapps.com/admin/#/loginFromLanding'
      })

      .constant('defaultLang', 'en')

      .constant('MapData',
        {
          'icon': '../assets/images/pages/support/marker.png',
          'markerIsraelCoords': { latitude: 31.7542788, longitude: 35.2185786},
          'markersUsaCoords': { latitude: 40.7202027, longitude: -74.0008645 },
          'zoom': 17,
          'styles': [
            {
              featureType: "landscape",
              stylers: [
                { color: '#eeddee' }
              ]
            },{
              featureType: "natural",
              stylers: [
                { hue: '#cae5e7' }
              ]
            },{
              featureType: "road",
              stylers: [
                { hue: '#5500aa' },
                { saturation: -70 }
              ]
            }
          ]
        }
      )
})();
