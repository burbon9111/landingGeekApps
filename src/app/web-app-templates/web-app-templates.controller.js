(function(){
    'use strict';

    angular
      .module('app.support')

      .controller('WebAppController', ['$scope', function($scope){
          let vm = this;
          vm.active = true;
        }]);
})();
