(function () {
    'use strict';

    angular
        .module('app.web-app-templates')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.web-app-templates', {
                    url: "/web-app-templates",
                    views: {
                        'content@app': {
                            templateUrl: "app/web-app-templates/web-app-templates.template.html",
                            controller: "WebAppController",
                            controllerAs: "webapp"
                        }
                    }
                });
        })
})();
