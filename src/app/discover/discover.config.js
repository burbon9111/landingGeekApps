(function () {
    'use strict';

    angular
        .module('app.discover')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.discover', {
                    url: "/discover",
                    views: {
                        'content@app': {
                            templateUrl: "app/discover/discover.template.html",
                        }
                    }
                });
        })
})();
