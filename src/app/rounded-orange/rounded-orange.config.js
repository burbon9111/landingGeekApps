(function () {
    'use strict';

    angular
        .module('app.rounded-orange')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.rounded-orange', {
                    url: "/rounded-orange",
                    views: {
                        'content@app': {
                            templateUrl: "app/rounded-orange/rounded-orange.template.html",
                        }
                    }
                });
        })
})();
