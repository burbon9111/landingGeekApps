(function () {
    'use strict';

    angular
        .module('app.dark-sea-japan')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.dark-sea-japan', {
                    url: "/dark-sea-japan",
                    views: {
                        'content@app': {
                            templateUrl: "app/dark-sea-japan/dark-sea-japan.template.html",
                        }
                    }
                });
        })
})();
