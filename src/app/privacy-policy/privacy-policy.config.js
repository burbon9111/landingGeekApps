(function () {
    'use strict';

    angular
        .module('app.privacy-policy')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.privacy-policy', {
                    url: "/privacy-policy",
                    views: {
                        'content@app': {
                            templateUrl: "app/privacy-policy/privacy-policy.template.html",
                        }
                    }
                });
        })
})();
