(function () {
    'use strict';

    angular
        .module('app.modern')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.modern', {
                    url: "/modern",
                    views: {
                        'content@app': {
                            templateUrl: "app/modern/modern.template.html",
                        }
                    }
                });
        })
})();
