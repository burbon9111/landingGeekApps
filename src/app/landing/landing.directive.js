(function(){
    'use strict';

    angular
      .module('app.landing')

      .directive('swiper', ['$parse', function ($parse) {
         return {
            restrict: 'A',
            scope: {
              swiper_: '=method'
            },
            link: function link(scope, elem, attrs, ctrl) {
              let windowWidth = window.innerWidth;
              let pageBreakpoint = 768;
              let swiperDirection;
              let speed;

              if(windowWidth < pageBreakpoint) {
                swiperDirection = 'horizontal';
                speed = 50;
              } else {
                swiperDirection = 'vertical';
                speed = 500;
              }

              let _target = attrs.target;
              let swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                direction: swiperDirection,
                slidesPerView: 1,
                paginationClickable: true,
                spaceBetween: 10,
                speed: speed,
                mousewheelControl: true,
                touchEventsTarget: _target,
                keyboardControl: true,
                mousewheelEventsTarged: _target,
                resizeReInit:true,
                effect: 'coverflow',
                autoplay: 8000,
                coverflow: {
                  rotate: 50,
                  stretch: 0,
                  depth: 10,
                  modifier: 3,
                  slideShadows : false
                },
                parallax: true
              });

              scope.swiper_(swiper);
            }
          };
        }])
})();
