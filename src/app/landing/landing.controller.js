(function(){
    'use strict';

    angular
        .module('app.landing')

        .controller('LandingController', ['$rootScope','$scope', 'Theme',
         function($rootScope, $scope, Theme){
            let vm = this;

            $rootScope.theme = Theme.Colors[0];

            vm.swipe = function(swiper) {
              swiper.on('onSlideChangeStart', function() {
                if (swiper.slides.length) {
                  $rootScope.theme = Theme.Colors[swiper.activeIndex];
                  $scope.$apply();
                }
              });
            }
        }]);
})();
