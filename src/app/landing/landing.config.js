(function () {
    'use strict';

    angular
        .module('app.landing')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.landing', {
                    url: "/landing",
                    views: {
                        'content@app': {
                            templateUrl: "app/landing/landing.template.html",
                            controller: "LandingController",
                            controllerAs: "landing"
                        }
                    }
                });
        })
})();
