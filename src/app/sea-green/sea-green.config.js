(function () {
    'use strict';

    angular
        .module('app.sea-green')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.sea-green', {
                    url: "/sea-green",
                    views: {
                        'content@app': {
                            templateUrl: "app/sea-green/sea-green.template.html",
                        }
                    }
                });
        })
})();
