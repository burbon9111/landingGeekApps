(function() {
    'use strict';

    var appDependencies = [
        'ui.router',
        'ngCookies',
        'LocalStorageModule',
        'ksSwiper',
        'app.components',
        'app.translations',
        'angular-google-analytics',
        'app.layout',
        'app.landing',
        'app.explore',
        'app.pricing',
        'app.clean-cut',
        'app.garde',
        'app.food',
        'app.modern',
        'app.sales',
        'app.look',
        'app.concrete',
        'app.sea-green',
        'app.rounded-orange',
        'app.leaves-light-green',
        'app.dark-sea-japan',
        'app.modern-look',
        'app.partners',
        'app.discover',
        'app.abstract',
        'app.web-app-templates',
        'app.web-app-templates-new',
        'app.support',
        'app.terms-and-conditions',
        'app.privacy-policy',
    ];
    angular.module('app', appDependencies);
})();
