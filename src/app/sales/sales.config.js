(function () {
    'use strict';

    angular
        .module('app.sales')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.sales', {
                    url: "/sales",
                    views: {
                        'content@app': {
                            templateUrl: "app/sales/sales.template.html",
                        }
                    }
                });
        })
})();
