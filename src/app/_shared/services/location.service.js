(function(){
  angular
   .module('app')

   .factory('location', ['$http', function($http) {
     return {

        getLocationData: function() {
          return $http({
              method: "GET",
              url: 'http://ip-api.com/json',
          })
            .then(function(response){
                return response.data;
            })
            .catch(function(response){
                return $q.reject(response);
            });
         }
      }
   }])
})();
