(function(){
  angular
   .module('app')

   .factory('analytics', ['$location', function($location) {
     return {
       setOutBoundLink: function(url) {
         //let currentPage = $location.absUrl().split('?')[0];
         let outBoundLink = 'https://geekapps.com/admin/#/setup/specification';

         ga('send', 'event', 'outbound', 'click', outBoundLink, {
           'transport': 'beacon',
           'hitCallback': function(){document.location = outBoundLink;}
         });
       }
     }
   }])
})();
