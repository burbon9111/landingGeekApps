(function(){
  angular
   .module('app')

   .factory('translation', ['$rootScope', '$translate', 'localstorage', 'Languages',
    function($rootScope, $translate, localstorage, Languages) {
     return {

       changeLanguage: function(key) {
         localstorage.set('lang', key);
         $rootScope.currentLang = key;
       },

       checkIsRTL: function(key) {
          let langId;

          langId = _.findIndex(Languages, function(o) {
             return o.code === key;
          });

          $rootScope.isRTL = Languages[langId].rtl;
       },

       validateLang: function(lang, defaultLang) {
         let currentLang = (!lang) ? defaultLang : lang;
         $translate.use(currentLang);
       }
     }
   }])
})();
