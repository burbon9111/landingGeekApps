(function () {
    'use strict';

    angular.module('app')
        .directive('customSelect', customSelectDirective)
        .controller('CustomSelectDirectiveController', customSelectDirectiveController);
    function customSelectDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/_shared/directives/custom-select/custom-select.html',
            scope: {
                items: '=items',
                selectedItem: '=selectedItem',
                animationDelay: '=animationDelay',
                selectLabel: '@selectLabel',
                nameProperty: '@nameProperty'
            },
            controller: 'CustomSelectDirectiveController',
            controllerAs: 'customSelectDirectiveCtrl',
            bindToController: true
        };
    }

    function customSelectDirectiveController($scope, $element, $attrs, $document, $timeout) {
        var vm = this;
        var animated;
        vm.toggleSelect = function () {
            if (animated) {
                return;
            }
            if (vm.selectIsOpen) {
                closeSelect();
                return;
            }
            vm.selectIsOpen = true;
            animated = true;
            $timeout(function () {
                animated = false;
                $document.bind('click', closeSelect);
            }, vm.animationDelay);
        };
        $timeout(function () {
            var options = document.getElementById('select-' + $scope.$id);
            angular.element(options).css({
                'animation-duration': (vm.animationDelay / 1000) + 's'
            });
        });
        function closeSelect() {
            if (animated && !vm.selectIsOpen) {
                return;
            }
            vm.selectIsOpen = false;
            animated = true;
            $timeout(function () {
                $document.unbind('click', closeSelect);
                animated = false;
            }, vm.animationDelay);
        }
    }

}());
