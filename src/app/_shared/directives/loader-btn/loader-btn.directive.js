(function(){
    'use strict';

    angular
        .module('app')
        .directive('loaderButton', loaderButtonDirective);
           function loaderButtonDirective($timeout, $q, $rootScope) {
               return {
                   restrict: 'A',
                   link: linkFunctions
               };
               function linkFunctions(scope, elem, attrs) {
                   var promise;

                   function loading() {
                       elem.addClass('load');
                       return $timeout(function () {
                           elem.addClass('loading');
                       }, 400);
                   }

                   function toggleBtn(elemClass, btnText, btnSymbol) {
                       removeLoading();
                       elem.addClass(elemClass).html(btnSymbol);
                       return $timeout(function () {
                           elem.removeClass(elemClass).html(btnText);
                           promise = null;
                       }, 2000);
                   }

                   function removeLoading() {
                       elem.removeClass('load loading');
                   }

                   elem.on('click', clickHandler);
                   function clickHandler(event) {
                       var btnText;
                       if (promise) {
                           return null;
                       }
                       $rootScope.loading = true;

                       btnText = elem.html();
                       elem.html('');
                       promise = $q.when(loading())
                           .then(function () {
                               return $timeout(function () {
                               });
                           })
                           .then(function () {
                               return $q.when(scope.$eval(attrs.loadAction, { $event: event }));
                           })
                           .then(function () {
                               return toggleBtn('complete', btnText, '✓');
                           })
                           .catch(function () {
                               return toggleBtn('error', btnText, 'x');
                           })
                           .finally(function () {
                               promise = null;
                               removeLoading();
                               $timeout(function () {
                                   $rootScope.loading = false;
                               });
                           });
                       return promise;
                   }
               }
           }

})();
