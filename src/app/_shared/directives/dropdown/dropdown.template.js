(function(module) {
try {
  module = angular.module('app.shared.directives.dropdown');
} catch (e) {
  module = angular.module('app.shared.directives.dropdown', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/_shared/directives/dropdown/dropdown.template.html',
    '');
}]);
})();
