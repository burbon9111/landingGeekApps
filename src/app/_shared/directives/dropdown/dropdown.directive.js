(function(){
    'use strict';

    angular
        .module('app')
        .directive('dropdown', function($timeout){
            return {
                restrict: 'A',
                require: 'ngModel',
                scope: {
                    list: '=dropdown',
                    ngModel: '='
                },
                template: '<div class="input-item dropdown" ng-click="open=!open" ng-class="{open:open}"> <div class="placeholder" ng-if="!selected">{{caption | translate}}</div><span>{{selected}}</span><div ng-repeat="item in list" class="list-item" ng-hide="!open" ng-click="update(item)" ng-class="{selected:selected===item}"><span>{{item}}</span></div><span class="title" style="top: 0px; z-index: {{list.length + 1}}"></span><span class="clickscreen" ng-hide="!open">&nbsp;</span></div>',
                replace: true,
                link: function(scope, elem, attrs, ngModel) {
                    scope.caption = attrs.caption;

                    scope.$watch('ngModel',function(){
                        scope.selected = ngModel.$modelValue;
                    });
                    scope.update = function(item) {
                        ngModel.$setViewValue(item);
                        ngModel.$render();
                    };
                }
            };
        });
})();
