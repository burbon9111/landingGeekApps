(function(){
    'use strict';

    angular
        .module('app.components.header')
        .controller('HeaderController', ['$rootScope', '$scope', '$translate', 'translation', 'Languages',
        function($rootScope, $scope, $translate, translation, Languages){
            let vm = this;
            vm.languages = Languages;
            vm.showLangs = false;

            vm.changeLanguage = function(key) {
              $translate.use(key);
              translation.changeLanguage(key);
              translation.checkIsRTL(key);
              vm.showLangs = false;
            }

            vm.toggle = function() {
              vm.showLangs = !vm.showLangs;
            }
        }]);
})();
