(function(){
    'use strict';

    angular
        .module('app.components.header')
        .directive('headerComponent', function(){
            return {
                restrict: 'AE',
                scope: true,
                controller: 'HeaderController',
                controllerAs: 'header',
                templateUrl: 'app/_components/header/header.template.html',
                link: function(){

                }
            }
        })
})();
