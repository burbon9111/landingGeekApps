(function(){
    'use strict';

    angular
        .module('app.components.footer')
        .directive('footerComponent', function(){
            return {
                restrict: 'AE',
                scope: true,
                controller: 'FooterController',
                controllerAs: 'footer',
                templateUrl: 'app/_components/footer/footer.template.html',
                link: function(){

                }
            }
        })

})();
