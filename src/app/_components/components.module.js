(function() {
    'use strict';

    /**
     *
     * Inject all components
     *
     * */

    angular.module('app.components',
      [
        'app.components.login-component',
        'app.components.footer',
        'app.components.header',
        'app.components.chat',
        'app.components.signalRHub'
      ]
    );
})();
