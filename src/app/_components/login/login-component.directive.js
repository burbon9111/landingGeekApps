(function(){
    'use strict';

    angular
      .module('app.components.login-component')
      .directive('loginComponent', function(){
          return {
              restrict: 'AE',
              scope: true,
              controller: 'LoginController',
              controllerAs: 'login',
              templateUrl: 'app/_components/login/login-component.template.html',
              link: function(){

              }
          }
      })
})();
