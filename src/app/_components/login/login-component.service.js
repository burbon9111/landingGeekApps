(function(){
    'use strict';
    angular
        .module('app.components.login-component')
        .service('LoginService', ['$http', '$q', 'server',
            function($http, $q, server){
                var exports = {
                    login: function(details){
                        return $http({
                            method: "POST",
                            url: server.createApp,
                            data: _.pick(details, 'email', 'password', 'country', 'phone')
                        })
                            .then(function(response){
                                return response;
                            })
                            .catch(function(error){
                                return $q.reject(error);
                            });
                    }
                };
                return exports;
            }
        ])
})();
