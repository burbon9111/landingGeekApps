(function () {
    'use strict';

    angular
    .module('app.components.login-component')
    .controller('LoginController', ['$rootScope', '$scope', '$translate', '$q', 'server', 'LoginService', 'Theme', 'CountriesList', 'analytics',
     function($rootScope, $scope, $translate, $q, server, LoginService,  Theme, CountriesList, analytics){
        let vm = this;
        let dialog;

        $rootScope.adminLink = server.admin;
        vm.existEmail = false;
        vm.countries = CountriesList;
        vm.error = false;
        vm.socialPrivacy = false;
        vm.selectedCountry = vm.countries[230];
        vm.user = {
           email: '',
           password: '',
           country: vm.selectedCountry.name,
           phone: '',
           code: vm.selectedCountry.dial_code
        };

        $scope.$watch('login.selectedCountry', function(value) {
          if(!value) {
            return;
          }
          vm.user.country = value.name;
          vm.user.code = value.dial_code;
        });

        vm.login = function () {
          vm.existEmail = false;
          vm.user.phone = vm.user.code + vm.user.number;
          vm.user.country = vm.user.country;

          if ($scope.loginForm.$invalid) {
              angular.forEach($scope.loginForm.$error, function (field) {
                angular.forEach(field, function(errorField){
                  errorField.$setTouched();
                })
              });
              return $q.reject();
          }
          return LoginService.login(vm.user)
              .then(function (response) {
                  vm.existEmail = false;
                  vm.error = false;
                  vm.user = {};
                  window.location = server.admin+'/'+response.data.accessToken+"/"+response.data.appId;
                  return $q.resolve();
              })
              .catch(function (error) {
                  if(error.data.message === "ERROR_ACCOUNT_WITH_SAME_EMAIL_ALREADY_EXISTS") {
                    vm.existEmail = true;
                  }
                  else {
                    vm.error = true;
                  }

                  return $q.reject();
              });
        };


        vm.socialLogin = function(provider) {
           $scope.loginForm.privacy.$setTouched();
           if(vm.user.privacy) {
             let redirectUrl = server.url + 'Views' + '/authcomplete2.html';
             let externalProviderUrl = server.url + "ExternalAccount/Signup?provider=" + provider
                 + "&redirect_url=" + redirectUrl
             window.addEventListener("message", authConnectResult);
             let authWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
           }
        };

        let authConnectResult = function(event) {
           vm.authData = event.data.access_token;
           analytics.setOutBoundLink();
           window.location = server.admin+'/'+vm.authData[0]+"/"+vm.authData[1];
           $scope.$apply();
        };
    }]);
}());
