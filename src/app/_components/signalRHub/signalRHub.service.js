(function(){
    'use strict';
    angular.module('app.components.signalRHub')
        .service('ChatHub', ['server', 'Hub', '$rootScope',
            function(server, Hub, $rootScope){
                return {
                    connectToHub: function(id){
                        //let audio = new Audio('assets/Notification.mp3');
                        let hub = new Hub('ChatHub', {
                            listeners:{
                                'AddMessage': function (message) {
                                    $rootScope.$emit('newMessageAdded', message);
                                    //audio.play();
                                }
                            },

                            queryParams:{
                                conversationId: id
                            },

                            rootPath: server.url +"signalr"
                        });
                    }
                }


            }])
})();
