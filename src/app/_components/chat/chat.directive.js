(function() {
    'use strict';

    angular.module('app.components.chat')
        .directive('chatBox', function(){
            return {
                restrict: "E",
                templateUrl: 'app/_components/chat/chat.template.html',
                controller:  'ChatBoxController',
                controllerAs: 'chatBox',
                bindToController: true
            }
        })
        .controller('ChatBoxController', ['$scope', '$element', '$attrs', 'ChatBoxService',
            function(scope, $element, $attrs, ChatBoxService){
                let vm = this;
                let audio = new Audio('assets/audio/Notification.mp3');

                vm.chatIsOpen = false;
                vm.myMessage = '';
                vm.messages = ChatBoxService.messages;
                scope.$watch('chatBox.messages', function(){
                    if(vm.messages.length > 1 && !vm.chatIsOpen){
                        vm.chatIsOpen = true;
                    }
                }, true);
                vm.sendMessage = function() {
                    if (vm.myMessage !== '' && Array.isArray(vm.messages)) {
                        audio.play();
                        ChatBoxService.sendMessage(vm.myMessage);
                        vm.messages.push({
                            user: true,
                            text: vm.myMessage
                        });
                    }
                    //scope.$broadcast('MessageAdding');
                    vm.myMessage = '';
                };

                $element.bind("keydown keypress", function(event) {
                    if(event.which === 13) {
                        scope.$apply(function(){
                            vm.sendMessage();
                        });
                        event.preventDefault();
                    }
                });


                scope.$on('MessagesLoaded', function(){
                    scope.$broadcast('MessageAdding');
                })
            }])
})();
