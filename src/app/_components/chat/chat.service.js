(function() {
    'use strict';

    angular.module('app.components.chat')
        .service('ChatBoxService', ['$q', '$http', '$rootScope', 'server', 'animatedScroll', 'ChatHub', function($q, $http, $rootScope, server, animatedScroll, ChatHub){
            let conversationId;
            $rootScope.$on('newMessageAdded', function(ev, message){
                exports.addNewMessage(message);
                $rootScope.$apply();
            });

            let message;

            let isSupportWorking = function() {
              let currentTime = new Date();
              let open = new Date();
              let closed = new Date();

              // 9am in UA
              open.setUTCHours(6);
              open.setUTCMinutes(0);
              open.setUTCMinutes(0);
              // 1am in UA (next day)
              closed.setUTCDate(currentTime.getUTCDate()+1);
              closed.setUTCHours(-2);
              closed.setUTCMinutes(0);
              closed.setUTCMinutes(0);

              return (currentTime > open && currentTime < closed) ? true : false;
            };

            message = isSupportWorking() ? 'CHAT-WRITE-TO-US' : 'CHAT-NO-WORKING';

            let exports = {
                messages: [
                    {
                        user:false,
                        text: message
                    }
                ],
                isSupportWorking: isSupportWorking(),
                sendMessage: function(message){
                    let deferred = $q.defer();
                    $http.post(server.url  + 'Chat/landing', {
                        body: message,
                        conversationId: conversationId
                    })
                        .then(function(response){
                            if(!conversationId){
                                ChatHub.connectToHub(response.data.conversationId);
                                conversationId = response.data.conversationId;
                            }

                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            let msg = error ? error.message : 'AN_INTERNAL_SERVER_ERROR';
                            deferred.reject(msg);
                        });
                    return deferred.promise;
                },
                addNewMessage: function(message){
                    animatedScroll.scroll('.chat-textarea');
                    exports.messages.push({
                        text: message.body,
                        user: message.isOwn
                    })

                }
            };
            return exports;
        }]);
})();
