'use strict';
  angular.module('app')
    .config(['$translateProvider', function($translateProvider){
        $translateProvider.translations("fr", {
          'LOGIN': 'Login FRENCH',
          'EMAIL-ADDRESS': 'Email Address',
          'NEW-PASSWORD': 'New Password',
          'CONFIRM-PASSWORD': 'Confirm Password',
          'COUNTRY': 'Country France',
          'CONTACT-NUMBER': 'Contact Number',
          'COUNTRY-CODE': 'Country Code',
          'NUMBER': 'NUMBER',
          'I-ACCEPT': 'I accept',
          'LETS-GO': 'Start Now!',
          'TERM-PRIVACY': 'The Terms and Conditions, & Privacy Policy',
          'SLIDE1-TITLE': 'Introducing the GeekTracker!',
          'SLIDE1-TEXT-PART1': 'The GeekTracker plugs you into your own business by letting you know about sales and appointments and messages entered by your clients.',
          'SLIDE1-TEXT-PART2': 'You can use the GeekTracker to discover and reach any of the other geekAppers out there.',
          'SLIDE2-TITLE': 'Choose the tools you need for your business',
          'SLIDE2-TEXT-PART1': 'Just think what else you need – we will help you sell more!',
          'SLIDE2-LIST-ITEM1': 'Do you want an online shop?',
          'SLIDE2-LIST-ITEM2': 'Do you need to have a reservations module?',
          'SLIDE2-LIST-ITEM3': 'Do you want to let your clients know about your special offers?',
          'SLIDE2-LIST-ITEM4': 'Do you need to chat with your clients?',
          'SLIDE3-TITLE': 'All you need to build your own app, shop and website step by step',
          'SLIDE3-TEXT-PART1': 'We’ve created our feature shop filled with tools for your business needs...',
          'SLIDE3-CHOOSE-TEMPLATE': 'Choose a Template',
          'SLIDE3-CHOOSE-STYLE-COLOR': 'Choose style and colour',
          'SLIDE4-TITLE': 'Ever wanted to reach more clients while trekking up Kilimanjaro or sitting on a beach?',
          'SLIDE4-LIST-ITEM1': 'Stay in contact with your customers on the go!',
          'SLIDE4-LIST-ITEM2': 'Don’t let your special deals get lost in the feed forests',
          'SLIDE4-LIST-ITEM3': 'One click and your post reaches all the platforms',
          'SLIDE4-LIST-ITEM4': 'iOS or android – we don’t discriminate',
          'SLIDE5-TITLE': 'Be part of the app revolution!',
          'SLIDE5-TEXT-PART1': 'Are you ready to join the app revolution? Of course you are ... let’s start building your very own amazing app',
          'SUPPORTS': 'Supports',
          'PRICING': 'PRICING',
          'EXPLORE': 'EXPLORE',
          'PARTNERS': 'PARTNERS',
          'SUPPORT': 'SUPPORT',
          'TEMPLATES': 'TEMPLATES',

          'ERROR_ACCOUNT_WITH_SAME_EMAIL_ALREADY_EXISTS': 'Emais exists'
        });
        $translateProvider.preferredLanguage('en');
  }]);
