(function () {
    'use strict';

    angular.module('app.translations', ['pascalprecht.translate', 'ngSanitize']);
}());
