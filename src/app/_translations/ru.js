'use strict';
  angular.module('app')
    .config(['$translateProvider', function($translateProvider){
        $translateProvider.translations("ru", {
          'LOGIN': 'Войти',
          'EMAIL-ADDRESS': 'Электронный адрес',
          'NEW-PASSWORD': 'Новый пароль',
          'CONFIRM-PASSWORD': 'Подтвердите пароль',
          'COUNTRY': 'Страна',
          'CONTACT-NUMBER': 'Контактный номер',
          'COUNTRY-CODE': 'Код страны',
          'NUMBER': 'Номер',
          'I-ACCEPT': 'Принимаю',
          'LETS-GO': 'Начать!',
          'TERM-CONDITIONS': 'The Terms and Conditions ',
          'PRIVACY-PRIVATE': ' Privacy Policy',
          'SLIDE1-TITLE': 'Представляем GeekTracker!',
          'SLIDE1-TEXT-PART1': 'GeekTracker позволяет осознать возможности своей компании, сообщая  вам о распродажах, назначении и введенных клиентами сообщениях.',
          'SLIDE1-TEXT-PART2': 'GeekTracker можно использовать для поиска других пользователей приложения и связи с ними.',
          'SLIDE2-TITLE': 'Выберите инструменты, необходимые для вашей компании',
          'SLIDE2-TEXT-PART1': 'Просто подумайте, что еще вам нужно - мы поможем вам продать больше!',
          'SLIDE2-LIST-ITEM1': 'Хотите онлайн-магазин?',
          'SLIDE2-LIST-ITEM2': 'Нужна система бронирования?',
          'SLIDE2-LIST-ITEM3': 'Хотите сообщать своим клиентам о специальных предложениях?',
          'SLIDE2-LIST-ITEM4': 'Нужно пообщаться в чате с клиентами?',
          'SLIDE3-TITLE': 'Все, что вам нужно, чтобы создать собственное приложение, магазин и веб-сайт шаг за шагом.',
          'SLIDE3-TEXT-PART1': 'Мы создали наш многофункциональный магазин с инструментами для нужд вашей компании…',
          'SLIDE3-CHOOSE-TEMPLATE': 'Выберите шаблон',
          'SLIDE3-CHOOSE-STYLE-COLOR': 'Выберите стиль и цвет',
          'SLIDE4-TITLE': 'Когда-либо хотели охватить больше клиентов, поднимаясь на Килиманджаро или отдыхая на пляже?',
          'SLIDE4-LIST-ITEM1': 'Будьте на связи с клиентами, где б вы ни были!',
          'SLIDE4-LIST-ITEM2': ' Ваши выгодные предложения не потеряются в джунглях ленты',
          'SLIDE4-LIST-ITEM3': 'Один клик - и ваше сообщение попадает на все платформы',
          'SLIDE4-LIST-ITEM4': 'iOS или Android: мы не делаем разницы',
          'SLIDE5-TITLE': 'Приобщитесь к революции в мире приложений!',
          'SLIDE5-TEXT-PART1': 'Вы готовы приобщиться к революции в мире приложений? Конечно, готовы… Начнем создавать ваше собственное прекрасное приложение',
          'SUPPORTS': 'Поддержка',
          'PRICING': 'Цены',
          'EXPLORE': 'Изучайте',
          'PARTNERS': 'Партнеры',
          'SUPPORT': 'Поддержка',
          'TEMPLATES': 'Шаблоны',
          'ERROR_ACCOUNT_WITH_SAME_EMAIL_ALREADY_EXISTS': 'учетная запись уже существует',
          'CLEAN-CUT-PAGE-TITLE': 'Clean Cut',
          'SECTION-START': 'Начните свое увлекательное путешествие с geekApps прямо сейчас!',
          'GARDE-PAGE-TITLE': 'Garde',
          'FOOD & BEVERAGE': ' Food & Beverage',
          'MODERN': 'Modern',
          'ABSTRACT-PAGE-TITLE': 'Abstract Light',
          'SALES-PAGE-TITLE': 'Sales Red',
          'LOOK-PAGE-TITLE': 'Modern Look',
          'CONCRETE-PAGE-TITLE': 'Concrete Grey',
          'SEA-GREEN-PAGE-TITLE': 'Sea Green',
          'ROUNDED-ORANGE-PAGE-TITLE': 'Rounded Orange',
          'LEAVES-LIGHT-GREEN-PAGE-TITLE': 'Leaves Light Green',
          'DARK-SEA-JAPAN-PAGE-TITLE': 'Dark Sea Japan',
          'MODERN-LOOK-PAGE-TITLE': 'Modern Look',
          'PARTNERS-PAGE-TITLE': 'Партнеры',
          'DISCOVER-PAGE-TITLE': 'Исследуйте',
          'TOP-AMAZING-GEEKAPPS': 'самое популярные geekApps приложения',
          'WEB-APP-TEMPLATES-PAGE-TITLE': 'ШАБЛОНЫ ДЛЯ ВЕБ САЙТОВ И ПРИЛОЖЕНИЯ',
          'WEB-TEMPLATES': 'Веб-шаблоны',
          'MOBILE-APPS': 'Мобильные приложения',
          'PRICING-PAGE-TITLE': 'Тарифные планы',
          'CHOOSE-PLANS-TEXT-LINE1': 'Выберите ежемесячный план',
          'CHOOSE-PLANS-TEXT-LINE2': 'Еще никогда так много не предлагалось за такие небольшую цену',
          'VIEW': 'View',
          'ERROR': 'Ошибка',
          'GIKI-SUBTITLE': 'Прекрасный выбор для новичков!',
          'VIP-SUBTITLE': 'Пусть вашу компанию заметят',
          'GOLD-SUBTITLE': 'Пусть вашу компанию заметят',
          'PREMIUM-SUBTITLE': 'Ваша компания популярна',
          'AMAZING-APP': 'Прекрасное предложение ',
          'DESKTOP-MOB': 'Веб и мобильный сайт',
          'BASIC-PACKAGE': 'Базовый пакет',
          'FREE-IN-GEEKTRACKER': 'бессплатно в GeekTracker',
          'ONLINE-SHOP': 'онлайн-магазин',
          'BOOKING-SYSTEM': 'онлайн-система бронирования',
          'MONTH': 'месяц',
          'CHOOSE-FROM-OUR-RANGE': 'Выбирайте среди ряда отличных шаблонов',
          'TEMPLATES-PAGE-DESCRIPTION-ONE': 'Вы уникальны. Выберите один из стилей шаблонов и персонализируйте его. Выбирайте среди разнообразия стилей: от успокаивающей палитры Карибского моря до поразительной смелости абстрактного света',
          'VIEW-TEMPLATES': 'Просмотреть шаблоны',
          'OUR-WEB-TEMPLATES': 'Наши веб-шаблоны абсолютно адаптивны и поддерживаются на всех устройствах',
          'TEMPLATES-PAGE-DESCRIPTION-TWO-SENTENSE1': 'Проснулись и хотите выразить свои чувства на веб-сайте?',
          'TEMPLATES-PAGE-DESCRIPTION-TWO-SENTENSE2': 'Организовываете зимнюю распродажу? Хотите хорошо провести время?',
          'TEMPLATES-PAGE-DESCRIPTION-TWO-SENTENSE3': 'Это праздник…',
          'TEMPLATES-PAGE-DESCRIPTION-TWO-SENTENSE4': 'Измените свой сайт одним нажатием кнопки',
          'TEMPLATES-PAGE-DESCRIPTION-THREE-SENTENSE1': 'Мгновенно просматривайте изменения на всех устройствах с ПК/ноутбуков/планшетов/устройств ipad и мобильных телефонов ( android и iOS).',
          'TEMPLATES-PAGE-DESCRIPTION-THREE-SENTENSE2': 'Хотите отменить изменения? Никаких проблем – просто нажмите еще раз, и ваш мир восстановлен',
          'SUPPORT-PAGE-TITLE': 'Поддержка',
          'CONTACT-US': 'Связаться с нами',
          'FOLLOW-PHRASE': 'Подпишитесь на канал geekApps, чтобы просматривать учебные фильмы',
          'CHAT-WRITE-TO-US': 'Здравствуйте! Мы тут, чтобы вам помочь! Что мы можем для вас сделать? :)',
          'COMMENT-PLACEHOLDER': 'Введите свое сообщение тут',
          'OUR-OFFICES': 'Наши офисы',
          'ONLINE-SUPPORT': 'Онлайн поддержка',
          'EXPLORE-PAGE-TITLE': 'Полнофункциональное приложение',
          'CHOOSE PAYMENT TEXT': 'Выберите тарифный план и приобщитесь к революции',
          'EXPLORE-PAGE-SECTION1-TITLE': 'Вы не одни',
          'EXPLORE-PAGE-SECTION1-DESCRIPTION': 'geekApps предлагает вам множество инструментов для компаний, которые помогут наладить процес роботы, увеличить продажи и присутствие в Интернете и установить связь с новыми клиентами. ',
          'EXPLORE-PAGE-SECTION1-SUBSECTION1-TITLE': 'Веб-сайт/сайт для мобильных устройств',
          'EXPLORE-PAGE-SECTION1-SUBSECTION2-TITLE': 'Персональный дизайн',
          'EXPLORE-PAGE-SECTION1-SUBSECTION3-TITLE': 'Панель администратора',
          'EXPLORE-PAGE-SECTION1-SUBSECTION4-TITLE': 'Geek Tracker',
          'EXPLORE-PAGE-SECTION1-SUBSECTION1-DESCRIPTION': 'Получите бесплатный адаптированный веб-сайт и приложение, которое можно полностью настроить (поддержка IOS/Android)',
          'EXPLORE-PAGE-SECTION1-SUBSECTION2-DESCRIPTION': 'Загрузите свои элементы бренда, логотип компании и выберите стиль шаблона',
          'EXPLORE-PAGE-SECTION1-SUBSECTION3-DESCRIPTION': 'Полноценное руководство приложением компании с помощью платформы веб-администратора',
          'EXPLORE-PAGE-SECTION1-SUBSECTION4-DESCRIPTION': 'Ваше мобильное приложение, которое помогает руководить своей компанией, отслеживать и развивать ее',
          'EXPLORE-PAGE-SECTION2-SUBTITLE': 'Хотя он базовый, вы не найдете другого базового приложения с таким количеством функций',
          'EXPLORE-PAGE-SECTION2-SUBSECTION1-TITLE': 'Push-оповещение',
          'EXPLORE-PAGE-SECTION2-SUBSECTION2-TITLE': 'Динамичная лента новостей',
          'EXPLORE-PAGE-SECTION2-SUBSECTION3-TITLE': 'Страница "О нас"',
          'EXPLORE-PAGE-SECTION2-SUBSECTION4-TITLE': 'Неограниченное количество загрузок',
          'EXPLORE-PAGE-SECTION2-SUBSECTION5-TITLE': 'Рассылка в социальные сети',
          'EXPLORE-PAGE-SECTION2-SUBSECTION6-TITLE': 'Полный доступ администратора',
          'EXPLORE-PAGE-SECTION2-SUBSECTION7-TITLE': 'Онлайн-чат',
          'EXPLORE-PAGE-SECTION2-SUBSECTION8-TITLE': 'Нативные приложения Android и IOS',
          'EXPLORE-PAGE-SECTION2-SUBSECTION9-TITLE': 'Ваша собственная галерея',
          'EXPLORE-PREMIUM-DESCRIPTION1': 'Полностью функциональный онлайн-магазин…',
          'EXPLORE-PREMIUM-DESCRIPTION1-FULL': 'Полнофункциональный онлайн-магазин для продажи неограниченного количества продуктов. Создавайте пользовательские категории, загружайте фотографии категорий, устанавливайте цены, размеры, подпозиции и т.д.',
          'EXPLORE-PREMIUM-DESCRIPTION2': 'Очень простая система бронирования…',
          'EXPLORE-PREMIUM-DESCRIPTION2-FULL': 'Очень простая система бронирования, а значит ваши клиенты смогут заказывать ваши услуги непосредственно в приложении.',
          'EXPLORE-PREMIUM-DESCRIPTION3': 'Все базовые функции',
          'EXPLORE-ONLINE BOOKING SYSTEM': 'СИСТЕМА БРОНИРОВАНИЯ ОНЛАЙН',
          'CLICK-ME': 'Нажать',
		      'EMAIL-ADDRESS-REQUIRED': 'Нужен электроный адрес',
          'EMAIL-ADDRESS-MUST-INCLUDE-SYMBOL': 'электронный адрес должен содержать символ @',
          'PASSWORD-REQUIRED': 'нужен пароль',
          'PASSWORD-MUST-INCLUDE-CHARACTERS': 'пароль должен содержать хотя бы 6 символов',
          'PASSWORD-ARENT-IDENTICAL': 'пароли не совпадают',
          'NUMBER-IS-REQUIRED': 'введите номер',
          'PLEASE-ENTER-MIN-SYMBOLS': 'введите хотя бы 7 символов',
          'PLEASE-ENTER-MAX-SYMBOLS': 'максимальное количество цифр 11',
          'OR-SIGN-UP-WITH': 'или войдите через',
    		  'MORE-TEMPLATES': 'Ожидайте еще больше шаблонов!',
    		  'CHAT-NO-WORKING': 'Мы сейчас спим, напишите нам в рабочее время или оставтьте Ваше сообщение'
        });
        $translateProvider.preferredLanguage('en');
  }]);
