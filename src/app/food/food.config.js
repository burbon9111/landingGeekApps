(function () {
    'use strict';

    angular
        .module('app.food')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.food', {
                    url: "/food",
                    views: {
                        'content@app': {
                            templateUrl: "app/food/food.template.html",
                        }
                    }
                });
        })
})();
