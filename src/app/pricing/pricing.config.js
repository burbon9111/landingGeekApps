(function () {
    'use strict';

    angular
        .module('app.pricing')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.pricing', {
                    url: "/pricing",
                    views: {
                        'content@app': {
                            templateUrl: "app/pricing/pricing.template.html",
                        }
                    }
                });
        })
})();
