(function () {
    'use strict';

    angular
        .module('app.partners')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.partners', {
                    url: "/partners",
                    views: {
                        'content@app': {
                            templateUrl: "app/partners/partners.template.html",
                        }
                    }
                });
        })
})();
