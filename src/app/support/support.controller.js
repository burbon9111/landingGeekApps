(function(){
    'use strict';

    angular
        .module('app.support')

        .controller('SupportController', ['$scope', 'ChatBoxService', 'MapData', function($scope, ChatBoxService, MapData){
            let vm = this;
            let markerIsraelCoords = MapData.markerIsraelCoords;
            let markersUsaCoords = MapData.markersUsaCoords;
            let markerIcon = MapData.icon;
            let zoom = MapData.zoom;

            vm.supportIsWorking = false;

            vm.mapIsrael = {
              center: markerIsraelCoords,
              zoom: zoom,
            };

            vm.mapUSA = {
              center: markersUsaCoords,
              zoom: zoom,
            };

            vm.markerIsrael = {
              id: 0,
              coords: markerIsraelCoords,
              options: {
                icon: markerIcon
              }
            };

            vm.markerUsa = {
              id: 1,
              coords: markersUsaCoords,
              options: {
                icon: markerIcon
              }
            };

            vm.mapOptions = {
              styles: MapData.styles
            }
        }]);
})();
