(function () {
    'use strict';

    angular
        .module('app.support')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.support', {
                    url: "/support",
                    views: {
                        'content@app': {
                            templateUrl: "app/support/support.html",
                            controller: "SupportController",
                            controllerAs: "support"
                        }
                    }
                });
        })
})();
