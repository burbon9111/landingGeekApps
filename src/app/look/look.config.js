(function () {
    'use strict';

    angular
        .module('app.look')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.look', {
                    url: "/look",
                    views: {
                        'content@app': {
                            templateUrl: "app/look/look.template.html",
                        }
                    }
                });
        })
})();
