(function () {
    'use strict';

    angular
        .module('app.leaves-light-green')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.leaves-light-green', {
                    url: "/leaves-light-green",
                    views: {
                        'content@app': {
                            templateUrl: "app/leaves-light-green/leaves-light-green.template.html",
                        }
                    }
                });
        })
})();
