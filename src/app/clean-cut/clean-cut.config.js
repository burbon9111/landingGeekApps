(function () {
    'use strict';

    angular
        .module('app.clean-cut')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.clean-cut', {
                    url: "/clean-cut",
                    views: {
                        'content@app': {
                            templateUrl: "app/clean-cut/clean-cut.template.html",
                        }
                    }
                });
        })
})();
