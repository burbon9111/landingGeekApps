(function () {
    'use strict';

    angular
        .module('app.abstract')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.abstract', {
                    url: "/abstract",
                    views: {
                        'content@app': {
                            templateUrl: "app/abstract/abstract.template.html",
                        }
                    }
                });
        })
})();
