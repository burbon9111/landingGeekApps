(function () {
    'use strict';

    angular
        .module('app.concrete')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app.concrete', {
                    url: "/concrete",
                    views: {
                        'content@app': {
                            templateUrl: "app/concrete/concrete.template.html",
                        }
                    }
                });
        })
})();
